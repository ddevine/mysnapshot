Place the script somewhere on your PATH using `echo $PATH` to see directories in your PATH or else just invoke the script manually from the current directory using `./mysnapshot [options]`

You may be required to give the file execute permissions using `chmod +x mysnapshot`.

Usage: mysnapshot [restore|backup] [db] [user] [name]

Example: Backup a databse called "django" with a MySQL user called "root" to a snapshot file called "snap1.sql"

	`mysnapshot backup django root snap1.sql`

Example: Restore a snapshot called "snap1.sql" to a database called "django" with a user called "root".

	`mysnapshot restore django root snap1.sql`